# PulseID-EUSIPCO



## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Dataset

The PulseID database is available upon request from the authors and agreement of EULA for research purposes.

### Installing

For running this project you need to install the requirements.txt

```
$ pip install -r requirements.txt
```

## Running the tests

First it is necessary to run `panda_dataframe_generator_PulseID.py` or `panda_dataframe_generator_TROIKA.py` to create
the respectives dataframes. Then the main script can be run following the next orientative command

```
python process_all.py --use_database PulseID --chunk_time 1 --batch_size 16 --num_epochs 20 --random_shuffles 150
--restart_ID_model True --use_class_weights False --ini_spk 1 --n_spks 1 --num_filters [6,6,6] --filter_sizes [50,30,20]
--earlystop True  --patience 7 --expID usr1_ct1_ep20_bs16_rs150_Es7_0320_nf666_fs503020 --nusr_train 35
```

## Built With

* [Python3.6](https://www.python.org/)
* [Keras](https://keras.io/)
* [Tensorflow](https://www.tensorflow.org/)

## Authors

* **Jordi Luque** - jordi.luqueserrano@telefonica.com
* **Guillem Cort�s** - guillem.cortes.sebastia@alu-etsetb.upc.edu
* **Carlos Segura** - carlos.seguraperales@telefonica.com
* **Joan Fabregat** - joan.fabregatserra@telefonica.com
* **Javier Esteban** - javier.estebanzarza@telefonica.com
* **Alexandre Maravilla** - aleixandre.maravillagirbes@telefonica.com


## License

This project is licensed under the MIT License - see the [License.md](License.md) file for details