#!/usr/bin/env python

import time
import numpy as np

def PPG_run(duration, fs, adc):
    TIME=1.0/fs           # Sampling time
    DELAY=0.005           # Delay between two consecutive samples
    delay = DELAY
    spins = TIME/DELAY    # Every 'spins' samples we must take its value
    DURATION = duration
    CHANNEL=0
    NUM_SAMPLES = int(DURATION/TIME)  # number of samples i total
    i=0

    # Allocate memory
    samples = np.empty((NUM_SAMPLES,2))
    t0 = time.time()

    while i < NUM_SAMPLES:
        start = time.time()
        spin = 0

        while spin < spins:
            time.sleep(delay)
            spin += 1

            Signal = adc.read(CHANNEL)      # Read from ADC
            currentTime = time.time()       # Update time
            value = (Signal * 3.3) / 1024   # Convert signal value to Volts

            samples[i,0] = str(currentTime-t0)  # Matrix: time [sec] | signal value [Volts]
            samples[i,1] = str(value)
            i+=1

            #SINCRO CODE CONTINUES
            end = time.time()
            adjust = (end-start)/spin-TIME  # Adjust the sampling
            delay -= adjust

            if delay < 0 :
                delay = 0

    return samples
