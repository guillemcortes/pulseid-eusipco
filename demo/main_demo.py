# !/usr/bin/env python
# Tensorflow: 1.3.0
# Keras: 2.0.8

from __future__ import print_function

# ---------- TO AVOID WARNINGS LIKE: -------- #
# The TensorFlow library wasn't compiled to use SSE4.1 instructions
# os.environ['TF_CPP_MIN_LOG_LEVEL']='3'
# ------------------------------------------- #

from MCP3008 import MCP3008
import sys, traceback
from PPG_rec import PPG_run
from score_per_chunk import *
from datetime import datetime
from keras import models
import numpy as np

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

def main():  # main function that starts the threads
    while True:
        print("Who is the user?")
        user = input("Who is the user? ")
        if user == '': # User name
            model_path = '.h5' # Path where the model is
            threshold = 0.5 # User's optimal threshold

        elif user == '': # User 2
            model_path = '.h5'
            threshold = 0.5 # User's optimal threshold

        print("Loading "+user+"'s model...")
        auth = 0
        TIME_OUT = 4
        iterations = 0
        test_time = 4  # Number of seconds of the experiment (p.e. 4 seconds decision)
        NN_model = models.load_model(model_path)
        chunk_time = 1
        fs = 200
        demo_samples = np.empty(shape=(test_time * fs, 2))
        BUS = 0
        DEVICE = 0
        adc = MCP3008(BUS, DEVICE)

        try:
            while True:
                demo_samples = np.empty(shape=(test_time * fs, 2))
                chunk_scores = np.empty(shape=(4, 1))
                print('\nRecording pulse samples...')
                demo_samples = PPG_run(test_time, fs, adc)

                veredict, chunk_scores, chunk_label = score_per_chunk(demo_samples[:, 1], NN_model, test_time,
                                                                      chunk_time, fs,
                                                                      threshold)

                print (datetime.now().strftime('%Y-%m-%d %H:%M:%S'))

                if veredict:
                    print(bcolors.OKGREEN + '¡¡¡ Hello ' + user + ' !!!'  + bcolors.ENDC)

                else:
                    print(bcolors.WARNING + 'User not verified' + bcolors.ENDC)
                
                # eprint('Scores: ', chunk_scores)
                # sys.stderr.flush()
                # print('Scores: ', chunk_scores,'           ',datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
                # print('Veredict: ', veredict)

        except KeyboardInterrupt:
            print(' - Reset requested')

if __name__ == "__main__":
   main()