import matplotlib
matplotlib.use('Agg')
print('Loading the necessary modules...')
from keras import models
import sys
import numpy as np


def load_model(NN_model_filepath):
    return models.load_model(NN_model_filepath)

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

def score_per_chunk(subject_signal, NN_model, test_time, chunk_time, fs, threshold):
    # Compute the scores for each chunk of data
    chunk_size = fs * chunk_time
    iterations = int(test_time / chunk_time)
    subject_signal_chunk = []

    for s in range(0,iterations):  # Iteration for each chunk of data
        subject_signal_chunk.append(subject_signal[s * chunk_size:(s + 1) * chunk_size])

    subject_signal_chunk = np.array(subject_signal_chunk)
    subject_signal_chunk = subject_signal_chunk.reshape(subject_signal_chunk.shape[0],subject_signal_chunk.shape[1], 1)
    chunk_scores = NN_model.predict(x = subject_signal_chunk, batch_size=1, verbose=0) # returns the scores

    # eprint('chunk_scores: \n', chunk_scores) # print the scores through the system error output
    # sys.stderr.flush()
    chunk_scores= np.squeeze(chunk_scores)

    #print('Scores vector:', chunk_scores)
    chunk_label = chunk_scores
    # Decide whether the subject is genuine or impostor
    chunk_label[np.where(chunk_label >= threshold)] = 1
    chunk_label[np.where(chunk_label < threshold)] = 0

    if np.count_nonzero(chunk_label) > 3:
        veredict = 1
    else:
        veredict = 0

    return veredict, chunk_scores, chunk_label
