import matplotlib
matplotlib.use('Agg') # necessary to avoid thread error in plt.savefig()
from matplotlib import pyplot as plt
import numpy as np

def Plot_ppg(path, filename, experiment_number):

  xx,yy = np.loadtxt(path+'.txt',dtype=float,delimiter='\t',unpack=True)
  
  print("--------- "+filename+" ---------")
  print("MAX Variation us: %.6f" % yy.max())
  print("STD: %.6f us" % yy.std())
  print("MEAN: %.6f us" % yy.mean())
    
  fig=plt.figure(figsize=(14,7))
  plt.plot(xx,yy)
  plt.title(filename)
  
  if "Ts" in filename:  # print Tsampling signal
    plt.xlabel('Samples')
    plt.ylabel('Time [us]')
    plt.text(100,yy.max()/2,'$\mu='+str(round(yy.mean(),3))+',\ \sigma='+str(round(yy.std(),3))+'$')
    
  elif "pulse" in filename: # print PPG signal
    plt.xlabel("Time [s]")
    plt.ylabel("Voltage [V]")
  
  plt.savefig(path + ".png", format = 'png', dpi=300)
  
  if int(experiment_number) == 0:
    plt.show()
  
  plt.close(fig)
  
