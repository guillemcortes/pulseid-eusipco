#!/usr/bin/env python

import time
from PPG_plot import Plot_ppg
from MCP3008 import MCP3008
import numpy as np
import os

def PPG_run(path, subject_number, session_number, experiment_number, duration):
    TIME=1.0/200          # Sampling time (sampling frequency = 200Hz)
    DELAY=0.005           # Delay between two consecutive samples
    delay = DELAY
    spins = TIME/DELAY    # Every 'spins' samples we must take its value
    DURATION = duration
    CHANNEL=0
    BUS=0
    DEVICE=0
    adc = MCP3008(BUS, DEVICE)
    NUM_SAMPLES = int(DURATION/TIME)  # number of samples i total
    PATH = path + "Subject_" + str(subject_number)+"/Session_" + str(session_number)
    i=0

    # Allocate memory
    samples = np.empty((NUM_SAMPLES,2))
    Tsampling = np.empty((NUM_SAMPLES,2)) # Tsampling only in order to verify the tune rate

    t0 = time.time()
    lastTime = t0
    print("\nPPG recording")

    while i < NUM_SAMPLES:
        start = time.time()
        spin = 0

        while spin < spins:
            time.sleep(delay)
            spin += 1

            Signal = adc.read(CHANNEL)      # Read from ADC
            currentTime = time.time()       # Update time
            value = (Signal * 3.3) / 1024   # Convert signal value to Volts

            samples[i,0] = str(currentTime-t0)  # Matrix: time [sec] | signal value [Volts]
            samples[i,1] = str(value)
            # Only to verify the tune rate
            Tsampling[i,0] = str(i)             # Matrix: sample number | Time error [us]
            Tsampling[i,1] = str((currentTime-lastTime-DELAY)*1000000)

            i+=1
            lastTime=currentTime

            #SINCRO CODE CONTINUES
            end = time.time()
            adjust = (end-start)/spin-TIME  # Adjust the sampling
            delay -= adjust

            if delay < 0 :
                delay = 0

    # Save vectors in disk
    filename_pulse="S" + str(subject_number)+ "_SS" + str(session_number) +"_Pulse_" + str(experiment_number)
    filename_Tsampling = "S" + str(subject_number)+ "_SS" + str(session_number) +"_Tsampling_" + str(experiment_number)

    # Create the necessary directories
    if not os.path.exists(PATH + "/Pulse/"):
        os.makedirs(PATH + "/Pulse/")

    if not os.path.exists(PATH + "/Tsampling/"):
        os.makedirs(PATH + "/Tsampling/")

    # Save signal values in a .txt
    np.savetxt(PATH + "/Pulse/" + filename_pulse + ".txt", samples, delimiter='\t')
    if int(experiment_number) != 0:
        np.savetxt(PATH + "/Tsampling/" + filename_Tsampling + ".txt", Tsampling, delimiter='\t')
        #Only in order to verify the tune rate

    print("+++ [DONE] PPG recording +++")

    # Plot
    print("Plotting Pulse in progress ...")
    Plot_ppg(PATH + "/Pulse/" + filename_pulse, filename_pulse, experiment_number)

    if int(experiment_number) != 0:
        print("Plotting Tsampling in progress ...")
        Plot_ppg(PATH + "/Tsampling/" + filename_Tsampling, filename_Tsampling, experiment_number)

    print("--- [DONE] Plotting ---")
