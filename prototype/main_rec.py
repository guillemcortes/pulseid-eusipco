from PPG_rec import PPG_run

subject_number=input("SUBJECT NUMBER [XXX]: ")
session_number=input("SESSION NUMBER [1 Morning ; 2 Afternoon]: ")
experiment_number=input("EXPERIMENT NUMBER [0..7]: ")

path = "../../data/PulseID_dataset/" # Dataset directory

# duration depends on the experiment number
if int(experiment_number) == 0: # Experiment 0 = visualize that the acquisition is good. All set is correct.
  duration = 5 #seconds

elif int(experiment_number) == 1:
  duration = 30 #seconds

elif int(experiment_number) == 2:
  duration = 30 #seconds

elif int(experiment_number) == 3:
  duration = 30 #seconds

elif int(experiment_number) == 4:
  duration = 30 #seconds

elif int(experiment_number) == 5:
  duration = 120 #seconds

elif int(experiment_number) == 6:
  duration = 300 #seconds

elif int(experiment_number) == 7:
  duration = 1200 #seconds

else: print("Index out of range")


if __name__ == '__main__':					# main function that starts the threads
    PPG_run(path, subject_number, session_number, experiment_number, duration)
