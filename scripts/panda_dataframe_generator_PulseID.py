import os
from scipy import signal, ndimage
import numpy as np
import pandas as pd

# --------------------- Initilalizations ---------------------- #
path = '' # DataBase .mat directory
lstFiles = []
lst = []
fitxers_test = []
fitxers = []
experiment = []
fs = 200
chunk_size = fs*10 #10 second chunk size
ppg_zscore = []
ppg_gdf = []

# ------- Get all .txt directories inside one folder ---------- #
for root, dirs, files in os.walk(path , topdown=True):
    for fichero in files:
        (nombreFichero, extension) = os.path.splitext(fichero)
        if(extension == ".txt"):
            lstFiles.append(os.path.join(root, fichero))		# lstFiles cointains every .txt full path
lstFiles.sort()

# ----------------- Creating Panda Dataframe ------------------ #
df = pd.DataFrame(columns=['Subject', 'ID', 'Experiment', 'Time', 'PPG', 'PPG_GDF'])

expermiments2use = ['1.txt', '2.txt', '3.txt', '4.txt', '5.txt', '7.txt']

for i in range(0,len(lstFiles)):
    parts = lstFiles[i].split('/')
    if parts[-2] == 'Pulse':
        experiment = parts[-1].split('_')
        if experiment[-1] in expermiments2use:					# Takes All experiments
            fitxers.append(lstFiles[i])


###===================== CREATION OF train DATAFRAME====================###
for i in np.arange(0, len(fitxers)):
    time_stamp = []
    parts = fitxers[i].split('/')								# parts is a vector which every position is one folder level of the path
    filename = parts[-1].split('_')
    if filename[0].lstrip('S') != 'Noisy':
        id_num = int(filename[0].lstrip('S'))
    exps = filename[-1].split('.')
    ppg_signal = np.loadtxt(fitxers[i], usecols=1)
    print('PPG_Signal', ppg_signal)
    time1 = len(ppg_signal)/fs
    for j in range(len(ppg_signal)):
        time_stamp.append(j/fs)

    time_np = np.array(time_stamp)
    print('measure', i,'->','ppg_signal size:',len(ppg_signal),'->',int(time1//60),'min',round(time1%60,2),'sec')

    # ------------------ Gaussian filtering-------------------- #
    ppg_gdf = ndimage.gaussian_filter1d(ppg_signal, sigma=1, order=0)

    df.loc[i] = [parts[-1], id_num, int(exps[0]), time_np, ppg_signal, ppg_gdf]		# Add line to the DataFrame

# ------------------ Z-score normalization -------------------- # 
# for row in df.itertuples():
#
# 	# print()
# 	# print('Row:', row[2])
# 	# print('Mean:',np.mean(row[2]))
# 	# print('Std:', np.std(row[2]))
#
# 	# df_zscore = (df - df.mean())/df.std()
# 	ppg_col = row[4]											# CHANGE IF NECESSARY
# 	ppg_mean = np.mean(ppg_col)									# z-score normalization
# 	ppg_std = np.std(ppg_col)
# 	zscore = np.array((ppg_col-ppg_mean)/ppg_std)
# 	ppg_zscore.append(zscore)
#
# df.loc[:,'PPG_zscore'] = ppg_zscore
# ------------------------------------------------------------- #


df.to_csv('DataFrame_PulseID.csv', sep='\t', encoding='utf-8')	# Export DataFrame to .csv
df.to_pickle('DataFrame_PulseID.pkl')	# Export DataFrame to pickle

# -------------------------- Plots ---------------------------- #
# subject = 3	# subject to plot
# plt.plot(df_train.iloc[subject]['Time'], df_train.iloc[subject]['PPG'],'C0', label='PPG', linewidth=0.5)
# plt.plot(df_test.iloc[subject]['Time'], df_test.iloc[subject]['PPG'],'C0', label='PPG', linewidth=0.5)
# # plt.plot(df.iloc[subject]['Time'], df.iloc[subject]['PPG_zscore'],'C1', label='PPG_zscore', linewidth=0.5)
# plt.plot(df_train.iloc[subject]['Time'], df_train.iloc[subject]['PPG_GDF'], 'C2', label='PPG_filtered gdf', linewidth=0.5)
# plt.plot(df_test.iloc[subject]['Time'], df_test.iloc[subject]['PPG_GDF'], 'C2', label='PPG_filtered gdf', linewidth=0.5)
# plt.xlabel('Time [sec]')
# plt.ylabel('Amplitude')
# plt.legend()
# plt.show()

